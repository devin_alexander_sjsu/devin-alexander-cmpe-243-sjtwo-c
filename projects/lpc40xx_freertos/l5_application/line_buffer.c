#include "line_buffer.h"
#include <stdio.h>

/**
 * Initialize *line_buffer_s with the user provided buffer space and size
 * Use should initialize the buffer with whatever memory they need
 * @code
 *  char memory[256];
 *  line_buffer_s line_buffer = { };
 *  line_buffer__init(&line_buffer, memory, sizeof(memory));
 * @endcode
 */
void line_buffer__init(line_buffer_s *buffer, void *memory, size_t size) {
  buffer->memory = memory;
  buffer->max_size = size;
  buffer->write_index = 0;
  buffer->read_index = 0;
}

// Adds a byte to the buffer, and returns true if the buffer had enough space to add the byte
bool line_buffer__add_byte(line_buffer_s *buffer, char byte) {
  bool buffer_had_space_for_another_byte = false;
  size_t new_write_index_to_try = buffer->write_index + 1;

  if (new_write_index_to_try <= buffer->max_size && buffer->write_index != buffer->max_size) {
    buffer_had_space_for_another_byte = true;
    buffer->memory[buffer->write_index] = byte;
    buffer->write_index = new_write_index_to_try;

    // If you are adding a byte to the buffer, then you are likely also not currently reading from it.
    buffer->read_index = 0;
  }
  return buffer_had_space_for_another_byte;
}

/**
 * If the line buffer has a complete line, it will remove that contents and save it to "char * line"
 * Note that the buffer may have multiple lines already in the buffer, so it will require multiple
 * calls to this function to empty out those lines
 *
 * The one corner case is that if the buffer is FULL, and there is no '\n' character, then you should
 * empty out the line to the user buffer even though there is no newline character
 *
 * @param line_max_size This is the max size of 'char * line' memory pointer
 */
bool line_buffer__remove_line(line_buffer_s *buffer, char *line, size_t line_max_size) {
  bool line_removed_from_buffer = false;
  char current_char_from_buffer_memory; // single char
  char temp_line[line_max_size];        // array of chars
  size_t actual_chars_before_new_line = 0;
  size_t current_buffer_index;

  // check to see if the read index has been reset to valid number less than max allow for more data to be read
  if (buffer->read_index <= buffer->max_size) {
    for (int index_offset = 0; index_offset < line_max_size; index_offset++) {
      // bookkeeping variables for the for loop
      current_buffer_index = buffer->read_index + index_offset;
      current_char_from_buffer_memory = buffer->memory[current_buffer_index];

      if (current_char_from_buffer_memory != '\n') {
        temp_line[index_offset] = current_char_from_buffer_memory; // save curr char to temp_line buffer

        if (current_buffer_index == buffer->max_size) {
          line_removed_from_buffer = true;
          // memcpy(line, &buffer->memory[buffer->read_index],
          memcpy(line, &temp_line, index_offset + 1); // +1 bc we don't see a '\n' char and need to copy last index
          buffer->read_index =
              buffer->max_size +
              1; // set it to an impossible number so it can't run this function again till the buffer is reset
          // This next line sees if the last space isn't null which indicates that there is just empty space for any
          // amount of chars after the first char
        } else if ((current_buffer_index == (buffer->read_index + line_max_size - 1)) &&
                   (current_char_from_buffer_memory != NULL)) {

          printf("index 2= %d\n", (int)index_offset);

          line_removed_from_buffer = true;
          memcpy(line, &temp_line, index_offset);    // +1 bc we don't see a '\n' char and need to copy last index
                                                     //   printf("Line buffer index 2= %d\n", (int)buffer->read_index);
          buffer->read_index = buffer->max_size + 1; // set it to an impossible number so it can't run this function
                                                     // again till the buffer is reset
        }
      } else if (current_char_from_buffer_memory == '\n') {
        buffer->read_index = current_buffer_index; // set this for next time the function is called
        line_removed_from_buffer = true;
        memcpy(line, &temp_line, index_offset); // NO +1 bc we see a '\n' char and don't need to copy it into our func

        if (current_buffer_index == buffer->max_size) {
          buffer->read_index =
              buffer->max_size +
              1; // set it to an impossible number so it can't run this function again till the buffer is reset
        }

        buffer->read_index += 1; // Skip one index because we need to account for the space where there was a '\n'
        break;                   // break out of for loop
      }
    }
  }

  return line_removed_from_buffer;
}