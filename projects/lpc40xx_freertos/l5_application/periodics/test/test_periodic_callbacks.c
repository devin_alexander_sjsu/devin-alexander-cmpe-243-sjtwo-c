#include <stdio.h>
#include <string.h>

#include "unity.h"

// Include the Mocks
// - This will not pull the REAL source code of these modules (such as board_io.c)
// - This will auto-generate "Mock" versions based on the header file
#include "Mockboard_io.h"
#include "Mockgpio.h"

// Add mock of your new code module
#include "MockFreeRTOS.h"
#include "Mockswitch_led_logic.h"
#include "Mocktask.h"
#include "periodic_callbacks.h"

#define PART_0
// #define PART_1
// #define PART_2
// #define PART_3

void setUp(void) {}

void tearDown(void) {}

void test__periodic_callbacks__initialize(void) {
  switch_led_logic__initialize_Expect();

  periodic_callbacks__initialize();
}

void test__periodic_callbacks__1Hz(void) {
  gpio_s gpio = {};
  board_io__get_led0_ExpectAndReturn(gpio);
  gpio__toggle_Expect(gpio);
  switch_led_logic__run_once_Expect();
  periodic_callbacks__1Hz(0);
  // vTaskDelay_Expect(1000);
}